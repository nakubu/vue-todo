import Vue from 'vue'
import Router from 'vue-router'
import hello from '@/components/hello'
import todo from '@/components/todo'

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: hello
    },
    {
      path: '/todo',
      name: 'todo',
      component: todo
    }
  ]
});
